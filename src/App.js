import React from 'react';
// import {} from 'react-router-dom';
import {
  Card,
} from 'semantic-ui-react';

import Router from './Router.js';

class App extends React.Component{

  render() {
    return(
      <div style={{margin:"0.5em"}} >
        <Card fluid >
          <Card.Content>
            <Card.Header>Car Search</Card.Header>
          </Card.Content>
        </Card>
        <Router/>
      </div>
    );
  }

}

export default App;