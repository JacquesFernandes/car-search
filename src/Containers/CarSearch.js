import React from 'react';
import axios from 'axios';
import _ from 'lodash';
import {
  Card,
  Header,
  Grid,
} from 'semantic-ui-react';

import backupData from '../Utils/backup_search.json';
import CarCard from '../Components/CarCard.js';

class CarSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state ={
      results: null,
    };

    this.monthlyCost = this.props.match.params.monthlyCost;
    this.generateCarCards = this.generateCarCards.bind(this);
  }

  componentDidMount() {
    if(!_.isNaN(Number(this.monthlyCost))){
      let url = `https://www.arnoldclark.com/used-cars/search.json?payment_type=monthly&amp;min_price=${Number(this.monthlyCost)-20}&amp;max_price=${Number(this.monthlyCost)+20}&amp;sort_order=monthly_payment_up`

      axios.get(url)
      .then(({ data }) => {
        this.setState({results: data.searchResults});
      })
      .catch((err) => {
        console.error("ERROR:",err);
        alert("CORS ISSUE: Arnold Clark servers rejected this request, using backup downloaded json with min_price 100 and max_price 150");
        let data = backupData.searchResults;
        data = _.sortBy(data, [(item) => item.salesInfo.pricing.monthlyPayment]); // most affordable first based on monthly payment amount
        data = _.slice(data, 0,6); // get top 6 most affordable
        console.log("DATA:",data);
        this.setState({results: data});
      })
    }
    else{
      alert("Sorry, this is an invalid price, please try again...");
      this.props.history.replace("/");
    }
  }

  generateCarCards() {
    let cards = _.map(this.state.results, (carData, index) => <Grid.Column key={"cc_"+index} >  
      <CarCard 
        imageUrl={carData.photos[0]} 
        name={carData.title.name} 
        variant={carData.title.variant} 
        stockReference={carData.stockReference} 
        monthlyPayment={carData.salesInfo.pricing.monthlyPayment}
        detailsUrl={carData.url} 
        enquiryUrl={carData.enquiryUrl} 
      /> 
    </Grid.Column> );

    return(cards);
  }

  render() {
    return(
      <Card fluid >
        <Card.Content>
          <Card.Header content="Step 3 - Search Cars" />
          <Header sub content={"Fetching top 6 most affordable cars around "+this.monthlyCost} />
        </Card.Content>
        <Card.Content>
          <Grid stackable relaxed >
            <Grid.Row columns="equal" stretched >
              {this.generateCarCards()}
            </Grid.Row>
          </Grid>
        </Card.Content>
      </Card>
    );
  }
}

export default CarSearch;
