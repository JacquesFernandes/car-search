import React from 'react';
import {
  Card,
} from 'semantic-ui-react';

import DetailsForm from '../Components/DetailsForm.js';

class Home extends React.Component {

  constructor(props){
    super(props);
    
    this.onCompletion = this.onCompletion.bind(this);
  }

  onCompletion() {
    this.props.history.push("/confirm")
  }

  render() {
    return(
      <Card fluid >
        <Card.Content>
          <Card.Header content="Step 1 - Setup" />
        </Card.Content>
        <Card.Content >
          <DetailsForm 
            onCompletion={this.onCompletion}
          />
        </Card.Content>
      </Card>
    );
  }
}

export default Home;