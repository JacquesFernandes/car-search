import React from 'react';
import _ from 'lodash';
import {
  Card,
  Table,
  Button,
} from 'semantic-ui-react';

import FirstPaymentRow from '../Components/FirstPaymentRow.js';
import FirstMonthPaymentRow from '../Components/FirstMonthPaymentRow.js';
import MiddleMonthPaymentRow from '../Components/MiddleMonthPaymentRow.js';
import LastMonthPaymentRow from '../Components/LastMonthPaymentRow.js';
import {
  getVehiclePrice,
  getFinanceOptionNumYears,
  getDeposit,
  getArrangementFee,
  getDeliveryDate,
  getCompletionFee,
} from '../Utils/storage.js';

class Confirmation extends React.Component {

  constructor(props) {
    super(props);
    
    this.monthlyCost = _.round((getVehiclePrice()-getDeposit()) / (12 * getFinanceOptionNumYears()), 2); // rounds UP (tiny bit of extra cash :P )

    let {firstDate, middleDates, completionDate} = this.paymentDateListGenerator();
    this.firstDate = firstDate;
    this.middleDates = middleDates;
    this.completionDate = completionDate;

    this.generateMiddleMonths = this.generateMiddleMonths.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleSearchClick = this.handleSearchClick.bind(this);
  }

  paymentDateListGenerator() {
    let deliveryDate = getDeliveryDate();
    let numberOfMonths = 12 * getFinanceOptionNumYears();

    let dates = _.times(numberOfMonths, (monthNumber) => { // returns the dates of each month that payment is due
      let dateCopy = new Date(deliveryDate.getTime()); // create copy of delivery date
      dateCopy.setMonth(dateCopy.getMonth()+monthNumber);
      return dateCopy;
    });

    dates.shift(); // remove first month entry (billing is supposed to start from month after delivery month)
    
    dates = _.map(dates, (date) => {
      let dateCopy = new Date(date.getTime());
      dateCopy.setDate(1); // start at first date of month
      while(dateCopy.getDay() !== 1) {
        dateCopy.setDate( dateCopy.getDate()+1 );
      }
      return this.dateStringGenerator(dateCopy);
    });
    

    let firstDate = dates.shift(); // fetch first month that the customer will be billed
    let completionDate = dates.pop();

    return({
      firstDate,
      middleDates: dates,
      completionDate
    });
  }

  dateStringGenerator(date = new Date()) {
    let conversion = {
      date: ((date.getDate() < 10)? "0" : "") + date.getDate(),
      month: ((date.getMonth() +1 < 10)? "0" : "") + (date.getMonth() +1),
      year: date.getFullYear(),
    };

    return(`${conversion.date}/${conversion.month}/${conversion.year}`);
  }

  generateMiddleMonths() {
    return(_.map(this.middleDates, (date, index) => <MiddleMonthPaymentRow key={"mmpr_"+index} date={date} cost={this.monthlyCost} />))
  }

  handleEditClick() {
    this.props.history.pop();
  }

  handleSearchClick() {
    this.props.history.push("/car-search/"+this.monthlyCost);
  }

  render() {
    return(
      <Card fluid >
        <Card.Content>
          <Card.Header>Step 2 - Confirm Payment Schedule</Card.Header>
        </Card.Content>
        <Card.Content>
          <Table celled textAlign="center" >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Date" />
                <Table.HeaderCell content="Payment Breakdown" />
                <Table.HeaderCell content="Total (GBP)" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <FirstPaymentRow
                currentDateString={this.dateStringGenerator()}
                deposit={getDeposit()}
              />
              <FirstMonthPaymentRow
                date={this.firstDate}
                arrangementFee={getArrangementFee()}
                monthlyCost={this.monthlyCost}
              />
              {this.generateMiddleMonths()}
              <LastMonthPaymentRow date={this.completionDate} completionFee={getCompletionFee()} monthlyCost={this.monthlyCost} />
            </Table.Body>
          </Table>
        </Card.Content>
        <Card.Content>
          <Button
            label="Edit Details"
            onClick={this.handleEditClick}
            icon="chevron left"
            floated="left"
            primary
          />
          <Button
            label="Search Cars"
            onClick={this.handleSearchClick}
            labelPosition="left"
            icon="chevron right"
            floated="right"
            positive
          />
        </Card.Content>
      </Card>
    );
  }
}

export default Confirmation;