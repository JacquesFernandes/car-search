/**
 * @returns {Number}
 */
function getVehiclePrice() {return Number(sessionStorage.getItem("vehiclePrice"))}

/**
 * @returns {Number}
 */
function getDeposit() {return Number(sessionStorage.getItem("deposit"))}

/**
 * @returns {Number}
 */
function getArrangementFee() {return Number(sessionStorage.getItem("arrangementFee"))}

/**
 * @returns {Number}
 */
function getCompletionFee() {return Number(sessionStorage.getItem("completionFee"))}

/**
 * @returns {Number}
 */
function getFinanceOptionNumYears() {return Number(sessionStorage.getItem("financeOptionNumYears"))}

/**
 * @returns {Date}
 */
function getDeliveryDate() {
  let date = sessionStorage.getItem("deliveryDate");
  date = (date)? new Date(date) : null;
  return date;
}

/**
 * setVehiclePrice
 * @param {Number} vehiclePrice - Base cost of the vehicle
 */
function setVehiclePrice(vehiclePrice) {sessionStorage.setItem("vehiclePrice", vehiclePrice)}

/**
 * setDeposit
 * @param {Number} deposit - Initial deposit
 */
function setDeposit(deposit) {sessionStorage.setItem("deposit", deposit)}

/**
 * setArrangementFee
 * @param {Number} arrangementFee - Inital arrangement fee to be paid along with the first month's payment
 */
function setArrangementFee(arrangementFee) {sessionStorage.setItem("arrangementFee", arrangementFee)}

/**
 * setCompletionFee
 * @param {Number} completionFee - Final completion fee to be paid along with the last month's payment
 */
function setCompletionFee(completionFee) {sessionStorage.setItem("completionFee", completionFee)}

/**
 * setFinanceOptionNumYears
 * @param {Number} financeOptionNumYears - Number of years chosen for the finance option
 */
function setFinanceOptionNumYears(financeOptionNumYears) {sessionStorage.setItem("financeOptionNumYears", financeOptionNumYears)}

/**
 * setDeliveryDate
 * @param {Date} deliveryDate - When the customer wants the vehicle delivered
 */
function setDeliveryDate(deliveryDate) {sessionStorage.setItem("deliveryDate", String(deliveryDate))} 

/**
 * 
 * @param {Object} data - Parameter object
 * @param {Number} data.vehiclePrice
 * @param {Number} data.deposit
 * @param {Number} data.arrangementFee
 * @param {Number} data.completionFee
 * @param {Number} data.financeOptionNumYears
 * @param {Date} data.deliveryDate
 */
function setFormDetails({ vehiclePrice, deposit, arrangementFee, completionFee, financeOptionNumYears, deliveryDate }) {
  setVehiclePrice(vehiclePrice);
  setDeposit(deposit);
  setArrangementFee(arrangementFee);
  setCompletionFee(completionFee);
  setFinanceOptionNumYears(financeOptionNumYears);
  setDeliveryDate(deliveryDate)
}

function clearFormDetails() {
  setVehiclePrice(null);
  setDeposit(null);
  setArrangementFee(null);
  setCompletionFee(null);
  setFinanceOptionNumYears(null);
  setDeliveryDate(null);
}

export {
  setFormDetails,
  clearFormDetails,
  getVehiclePrice,
  getDeposit,
  getArrangementFee,
  getCompletionFee,
  getFinanceOptionNumYears,
  getDeliveryDate,
  setVehiclePrice,
  setDeposit,
  setArrangementFee,
  setCompletionFee,
  setFinanceOptionNumYears,
  setDeliveryDate,
};