import React from 'react';
import {
  Route,
  BrowserRouter,
} from 'react-router-dom';

import Home from './Containers/Home.js';
import Confirmation from './Containers/Confirmation.js';
import CarSearch from './Containers/CarSearch.js';

class Router extends React.Component {

  render() {
    return(
      <BrowserRouter>
        <div>
          <Route exact path="/" component={Home} />
          <Route exact path="/confirm" component={Confirmation} />
          <Route exact path="/car-search/:monthlyCost" component={CarSearch} />
        </div>
      </BrowserRouter>
    );
  }

}

export default Router;