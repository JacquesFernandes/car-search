import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Table,
} from 'semantic-ui-react';

class FirstMonthPaymentRow extends React.Component {

  render() {

    let { date, arrangementFee, monthlyCost } = this.props;
    
    return(
      <Table.Row>
        <Table.Cell>
          {date} (First Payment)
        </Table.Cell>
        <Table.Cell>
          <Table >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Label" />
                <Table.HeaderCell content="Amount (GBP)" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell content="Arrangement Fee" />
                <Table.Cell content={arrangementFee} />
              </Table.Row>
              <Table.Row>
                <Table.Cell content="Monthly Payment" />
                <Table.Cell content={monthlyCost} />
              </Table.Row>
              <Table.Row active >
                <Table.Cell content="Total"  />
                <Table.Cell content={_.round(arrangementFee + monthlyCost,2)} />
              </Table.Row>
            </Table.Body>
          </Table>
        </Table.Cell>
        <Table.Cell content={_.round(arrangementFee + monthlyCost,2)} />
      </Table.Row>
    );
  }
}

FirstMonthPaymentRow.propTypes = {
  date: PropTypes.string,
  arrangementFee: PropTypes.number,
  monthlyCost: PropTypes.number,
};

export default FirstMonthPaymentRow;