import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Table,
} from 'semantic-ui-react';

class LastMonthPaymentRow extends React.Component {

  render() {

    let { date, completionFee, monthlyCost } = this.props;
    
    return(
      <Table.Row>
        <Table.Cell>
          {date} (Last Payment)
        </Table.Cell>
        <Table.Cell>
          <Table >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Label" />
                <Table.HeaderCell content="Amount (GBP)" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell content="Completion Fee" />
                <Table.Cell content={completionFee} />
              </Table.Row>
              <Table.Row>
                <Table.Cell content="Monthly Payment" />
                <Table.Cell content={monthlyCost} />
              </Table.Row>
              <Table.Row active >
                <Table.Cell content="Total"  />
                <Table.Cell content={_.round(completionFee + monthlyCost,2)} />
              </Table.Row>
            </Table.Body>
          </Table>
        </Table.Cell>
        <Table.Cell content={_.round(completionFee + monthlyCost,2)} />
      </Table.Row>
    );
  }
}

LastMonthPaymentRow.propTypes = {
  date: PropTypes.string,
  completionFee: PropTypes.number,
  monthlyCost: PropTypes.number,
};

export default LastMonthPaymentRow;