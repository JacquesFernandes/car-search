import React from 'react';
import {
  Table
} from 'semantic-ui-react';

class MiddleMonthPaymentRow extends React.Component {
  
  render() {
    let { date, cost } = this.props;

    return(
      <Table.Row>
        <Table.Cell>
          {date}
        </Table.Cell>
        <Table.Cell>
          <Table >
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Label" />
                <Table.HeaderCell content="Amount (GBP)" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell content="Monthly Payment" />
                <Table.Cell content={cost} />
              </Table.Row>
              <Table.Row active >
                <Table.Cell content="Total"  />
                <Table.Cell content={cost} />
              </Table.Row>
            </Table.Body>
          </Table>
        </Table.Cell>
        <Table.Cell content={cost} />
      </Table.Row>
    )
  }
}

export default MiddleMonthPaymentRow;