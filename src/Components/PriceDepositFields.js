import React from 'react';
import _ from 'lodash';
import {
  Form,
  Message,
} from 'semantic-ui-react';

const FALLBACK_PRICE = 1.00;

class PriceDepositFields extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      price: this.props.initialPrice || FALLBACK_PRICE,
      deposit: this.props.initialDeposit || this.calculateMinDeposit(FALLBACK_PRICE),
    };

    this.onPriceChange = this.onPriceChange.bind(this);
    this.onDepositChange = this.onDepositChange.bind(this);
    this.calculateMinDeposit = this.calculateMinDeposit.bind(this);
    this.isPriceValid = this.isPriceValid.bind(this);
    this.isDepositValid = this.isDepositValid.bind(this);
  }

  proxyUpdateParentNode() {
    if(_.isFunction(this.props.onChange)){
      this.props.onChange({
        newPrice: this.state.price, 
        newDeposit: this.state.deposit,
        depositIsValid: this.isDepositValid(),
        priceIsValid: this.isPriceValid(),
      });
    }
  }

  onPriceChange(event, { value }) {
    value = Number(value);
    if(!_.isNaN(value)){
      this.setState({
        price: value,
        deposit: (value !== 0 && this.state.deposit < this.calculateMinDeposit(value))? this.calculateMinDeposit(value) : this.state.deposit
      }, () => {
        this.proxyUpdateParentNode();
      });
    }
  }

  onDepositChange(event, { value }) {
    value = Number(value);
    if(!_.isNaN(value) && this.calculateMinDeposit(value) <= value){
      this.setState({deposit: value}, () => {
        this.proxyUpdateParentNode();
      });
    }
  }

  calculateMinDeposit(price) {
    price = Number(price);
    if(!_.isNaN(price)) {
      return( 0.15 * price );
    }
    else{
      return(1.00);
    }
  }

  isPriceValid() {
    if(this.state.price){
      let price = Number(this.state.price);
      return( !_.isNaN(price) && price > 0 );
    }
    else{
      return false;
    }
  }

  isDepositValid() {
    return ( this.state.deposit >= this.calculateMinDeposit(this.state.price) );
  }

  render() {
    return(
      <div>
        <Form.Input 
          label="Vehicle Price"
          type="number"
          onChange={this.onPriceChange}
          defaultValue={this.state.price}
          min={0}
        />
        <Message 
          visible={!this.isPriceValid()}
          content="The Vehicle Price has to be a number greater than 0"
          error
        />
        <Form.Input 
          label="Deposit (min. 15% of price)"
          type="number"
          onChange={this.onDepositChange}
          value={this.state.deposit}
          min={0}
        />
        <Message
          visible={!this.isDepositValid()}
          content={"The Deposit has to be greater than (or equal to) "+this.calculateMinDeposit(this.state.price)}
          error
        />
      </div>
    );
  }

}

export default PriceDepositFields;