import React from 'react';
import _ from 'lodash';
import {
  Form,
  Message
} from 'semantic-ui-react';

class DateMessageField extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      value: this.props.initialValue,
    };
    
    let date = new Date();
    this.currentDate = {
      date: date.getDate(),
      month: date.getMonth()+1,
      year: date.getUTCFullYear(),
    };

    this.onChange = this.onChange.bind(this);
    this.isFieldValid = this.isFieldValid.bind(this);
    this.proxyUpdateParentNode = this.proxyUpdateParentNode.bind(this);
  }

  proxyUpdateParentNode() {
    if(_.isFunction(this.props.onChange)){

      this.props.onChange({
        newValue: this.state.value,
        isValid: this.isFieldValid(),
      });
    }
  }

  onChange(event, { value }) {
    let newDate = new Date(value);
    this.setState({ value: newDate }, () => {
      this.proxyUpdateParentNode();
    });
  }

  isFieldValid(value = this.state.value) {
    if(value !== null){
      let date = (_.isDate(value))? value : new Date(value);
      if(date.getTime() <= Date.now()) {
        return false;
      }
      else{
        return true;
      }
    }
    else{
      return false;
    }
  }

  render() {
    return(
      <div>
        <Form.Input
          label="Delivery Date"
          type="date"
          min={`${this.currentDate.year}-${this.currentDate.month}-${this.currentDate.date}`}
          defaultValue={(_.isDate(this.state.value))? `${this.state.value.getUTCFullYear()}-${this.state.value.getMonth() + 1}-${this.state.value.getDate()}` : null}
          onChange={this.onChange}
        />
        <Message
          content="The date is invalid"
          visible={!this.isFieldValid()}
          error
        />
      </div>
    );
  }
}

export default DateMessageField;