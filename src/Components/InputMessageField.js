import React from 'react';
import _ from 'lodash';
import {
  Form,
  Message
} from 'semantic-ui-react';

class InputMessageField extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      value: this.props.initialValue,
    };

    this.isFieldValid = this.isFieldValid.bind(this);
    this.onFieldChange = this.onFieldChange.bind(this);
    this.proxyUpdateParentNode = this.proxyUpdateParentNode.bind(this);
  }

  proxyUpdateParentNode() {
    if(_.isFunction(this.props.onChange)){
      this.props.onChange({
        newValue: this.state.value,
        isValid: this.isFieldValid(),
      });
    }
  }

  isFieldValid(value = this.state.value) {
    if(value && !_.isNaN(value) && value > 0) {
      return true;
    }
    else{
      return false;
    }
  }

  onFieldChange(event, { value }) {
    this.setState({ value }, () => {
      this.proxyUpdateParentNode();
    });
  }

  render() {
    return(
      <div>
        <Form.Input
          label={this.props.fieldName}
          type="number"
          defaultValue={this.state.value}
          onChange={this.onFieldChange}
          min={1.00}
        />
        <Message
          visible={!this.isFieldValid()}
          content={"The "+(this.props.fieldName || "amount")+" has to be a number greater than 0 (zero)"}
          error
        />
      </div>
    );
  }

}

export default InputMessageField;