import React from 'react';
import _ from 'lodash';
import {
  Form,
  Message,
} from 'semantic-ui-react';

class FinanceOptionsMessageField extends React.Component {
  constructor(props) {
    super(props);
    
    this.state ={
      value: this.props.initialValue,
    };

    this.yearOptions = [1, 2, 3]; // years for finance options

    this.handleChange = this.handleChange.bind(this);
    this.generateYearOptions = this.generateYearOptions.bind(this);
    this.proxyUpdateParentNode = this.proxyUpdateParentNode.bind(this);
  }

  proxyUpdateParentNode() {
    if(_.isFunction(this.props.onChange)){

      this.props.onChange({
        newValue: this.state.value,
        isValid: this.isFieldValid(),
      });
    }
  }

  handleChange(event, { value }) {
    this.setState({ value }, () => {
      this.proxyUpdateParentNode();
    });
  }

  generateYearOptions() {
    let options = this.yearOptions.map((value, index) => ({
      key: "fo_"+index,
      text: value+" year"+((value > 1)? "s" : ""),
      value
    }));

    return(options);
  }

  isFieldValid() {
    if(this.state.value && _.includes(this.yearOptions, this.state.value)) {
      return true;
    }
    else{
      return false;
    }
  }

  render() {
    return(
      <div>
        <Form.Select 
          label="Finance Option"
          options={this.generateYearOptions()}
          placeholder="Select a Finance Option"
          defaultValue={this.state.value}
          onChange={this.handleChange}
        />
        <Message
          content="This Finance Option is not valid"
          visible={!this.isFieldValid()}
          error
        />
      </div>
    );
  }
}

export default FinanceOptionsMessageField;