import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
} from 'semantic-ui-react';

class FirstPaymentRow extends React.Component {

  render() {
    return(
      <Table.Row>
        <Table.Cell content={this.props.currentDateString+" (Today)"} />
        <Table.Cell>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell content="Label" />
                <Table.HeaderCell content="Amount (GBP)" />
              </Table.Row>
            </Table.Header>
            <Table.Body>
              <Table.Row>
                <Table.Cell content="Deposit" />
                <Table.Cell content={this.props.deposit} />
              </Table.Row>
              <Table.Row active >
                <Table.Cell content="Total" />
                <Table.Cell content={this.props.deposit} />
              </Table.Row>
            </Table.Body>
          </Table>
        </Table.Cell>
        <Table.Cell content={this.props.deposit} />
      </Table.Row>
    );
  }
}

FirstPaymentRow.propTypes = {
  currentDateString: PropTypes.string,
  deposit: PropTypes.number,
}

export default FirstPaymentRow;