import React from 'react';
import _ from 'lodash';
import {
  Form,
  Message,
  Button,
  Divider,
} from 'semantic-ui-react';

import PriceDepositFields from './PriceDepositFields.js';
import InputMessageField from './InputMessageField.js';
import DateMessageField from './DateMessageField.js';
import FinanceOptionsMessageField from './FinanceOptionsMessageField.js';
import { 
  setFormDetails,
  getVehiclePrice,
  getDeposit,
  getArrangementFee,
  getCompletionFee,
  getDeliveryDate,
  getFinanceOptionNumYears,
} from '../Utils/storage.js';

class DetailsForm extends React.Component {

  constructor(props) {
    super(props);

    this.price=getVehiclePrice() || 1.00;
    this.deposit=getDeposit() || 0.15;
    this.arrangementFee=getArrangementFee() || 88.00;
    this.completionFee=getCompletionFee() || 20.00;
    this.financeOption=getFinanceOptionNumYears() || null;
    this.deliveryDate=getDeliveryDate() || null;
    
    this.state ={
      priceIsValid: true,
      depositIsValid: true,
      arrangementFeeIsValid: true,
      completionFeeIsValid: true,
      deliveryDateIsValid: (this.deliveryDate)? true : false,
      financeOptionIsValid: (this.financeOption)? true : false,
    };

    this.onPriceDepositChange = this.onPriceDepositChange.bind(this);
    this.onArrangementFeeChange = this.onArrangementFeeChange.bind(this);
    this.onCompletionFeeChange = this.onCompletionFeeChange.bind(this);
    this.onDeliveryDateChange = this.onDeliveryDateChange.bind(this);
    this.onFinanceOptionChange = this.onFinanceOptionChange.bind(this);
    this.isFormValid = this.isFormValid.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  onPriceDepositChange({ newPrice, newDeposit, depositIsValid, priceIsValid }) {
    let stateUpdateObject = {};
    
    this.price = newPrice;
    this.deposit = newDeposit;

    if(priceIsValid !== this.state.priceIsValid){
      stateUpdateObject.priceIsValid = priceIsValid;
    }

    if(depositIsValid !== this.state.depositIsValid){
      stateUpdateObject.depositIsValid = depositIsValid;
    }

    if(!_.isEmpty(stateUpdateObject)){
      this.setState(stateUpdateObject);
    }
    
  }

  onArrangementFeeChange({ newValue, isValid }) {
    this.arrangementFee = newValue;

    if(this.state.arrangementFeeIsValid !== isValid) {
      this.setState({arrangementFeeIsValid: isValid});
    }
  }

  onCompletionFeeChange({ newValue, isValid }) {
    this.completionFee = newValue;

    if(this.state.completionFeeIsValid !== isValid) {
      this.setState({completionFeeIsValid: isValid});
    }
  }

  onDeliveryDateChange({ newValue, isValid }) {
    this.deliveryDate = newValue;

    if(this.state.deliveryDateIsValid !== isValid) {
      this.setState({ deliveryDateIsValid: isValid });
    }
  }

  onFinanceOptionChange({ newValue, isValid }) {
    this.financeOption = newValue;

    if(this.state.financeOptionIsValid !== isValid){
      this.setState({ financeOptionIsValid: isValid });
    }
  }

  isFormValid() {
    return(
      this.state.priceIsValid &&
      this.state.depositIsValid &&
      this.state.arrangementFeeIsValid &&
      this.state.completionFeeIsValid &&
      this.state.deliveryDateIsValid &&
      this.state.financeOptionIsValid
    );
  }

  submitForm() {
    if(this.isFormValid()){
      setFormDetails({
        vehiclePrice: this.price,
        deposit: this.deposit,
        arrangementFee: this.arrangementFee,
        completionFee: this.completionFee,
        financeOptionNumYears: this.financeOption,
        deliveryDate: this.deliveryDate,
      });
      
      if(_.isFunction(this.props.onCompletion)) {
        this.props.onCompletion();
      }
    }
    else{
      console.error("ERROR: Tried to set an invalid form!");
    }
  }

  render() {
    let globalValidCheck = this.isFormValid();
    
    return(
      <Form>
        <PriceDepositFields
          initialPrice={this.price}
          initialDeposit={this.deposit}
          onChange={this.onPriceDepositChange}
        />
        <InputMessageField
          fieldName="Arrangement Fee"
          initialValue={this.arrangementFee}
          onChange={this.onArrangementFeeChange}
        />
        <InputMessageField
          fieldName="Completion Fee"
          initialValue={this.completionFee}
          onChange={this.onCompletionFeeChange}
        />
        <DateMessageField
          initialValue={this.deliveryDate}
          onChange={this.onDeliveryDateChange}
        />
        <FinanceOptionsMessageField
          initialValue={this.financeOption}
          onChange={this.onFinanceOptionChange}
        />
        <Divider />
        <Message 
          content="There is an error with the form, please check and fix any errors"
          visible={!globalValidCheck}
          error
        />
        <Button
          label="Confirm"
          onClick={this.submitForm}
          disabled={!globalValidCheck}
          labelPosition="left"
          icon="chevron right"
          floated="right"
          positive
        />
      </Form>
    );
  }

}

export default DetailsForm;