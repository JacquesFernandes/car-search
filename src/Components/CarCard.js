import React from 'react';
import {
  Card,
  Image,
  Header,
  Button,
} from 'semantic-ui-react';

class CarCard extends React.Component {

  constructor(props) {
    super(props);

    this.handleDetailsClick = this.handleDetailsClick.bind(this);
    this.handleEnquireClick = this.handleEnquireClick.bind(this);
  } 

  handleDetailsClick() {
    window.open("https://www.arnoldclark.com"+this.props.detailsUrl);
  }

  handleEnquireClick() {
    window.open("https://www.arnoldclark.com"+this.props.enquiryUrl);
  }

  render() {
    return(
      <Card>
        <Image src={this.props.imageUrl || "https://react.semantic-ui.com/images/wireframe/image.png"} />
        <Card.Content>
          <Header sub content="Name" />
          {this.props.name}
          <Header sub content="Variant" />
          {this.props.variant}
          <Header sub content="Stock Reference" />
          {this.props.stockReference}
          <Header sub content="Monthly Payment" />
          GBP {this.props.monthlyPayment}
        </Card.Content>
        <Card.Content>
          <Button
            content="Details"
            onClick={this.handleDetailsClick}
            primary
            fluid
          />
        </Card.Content>
        <Card.Content>
          <Button
            content="Enquire"
            onClick={this.handleEnquireClick}
            positive
            fluid
          />
        </Card.Content>
      </Card>
    );
  }
}

export default CarCard;